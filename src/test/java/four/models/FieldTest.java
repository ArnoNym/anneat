package four.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class FieldTest {
    @Test
    public void testIsWinning_no_1() {
        Field field = new Field();

        int row = 0;
        int column = 3;
        Player player = Player.PLAYER_1;

        field.put(column, player);

        assertFalse(field.isWinning(row, column, 1, 0, player));
        assertFalse(field.isWinning(row, column, 0, 1, player));
        assertFalse(field.isWinning(row, column, 1, 1, player));
        assertFalse(field.isWinning(row, column, 1, -1, player));
        assertFalse(field.isWinning(row, column));
    }
    @Test
    public void testIsWinning_yes_1() {
        Field field = new Field();

        field.put(2, Player.PLAYER_1);
        assertFalse(field.isWinning(0, 2));
        field.put(2, Player.PLAYER_1);
        assertFalse(field.isWinning(0, 2));
        assertFalse(field.isWinning(1, 2));
        field.put(2, Player.PLAYER_1);
        assertFalse(field.isWinning(0, 2, 1, 0, Player.PLAYER_1));
        assertFalse(field.isWinning(1, 2, 1, 0, Player.PLAYER_1));
        assertFalse(field.isWinning(2, 2, 1, 0, Player.PLAYER_1));
        assertFalse(field.isWinning(0, 2));
        assertFalse(field.isWinning(1, 2));
        assertFalse(field.isWinning(2, 2));
        field.put(2, Player.PLAYER_1);
        assertTrue(field.isWinning(0, 2, 1, 0, Player.PLAYER_1));
        assertTrue(field.isWinning(1, 2, 1, 0, Player.PLAYER_1));
        assertTrue(field.isWinning(2, 2, 1, 0, Player.PLAYER_1));
        assertTrue(field.isWinning(3, 2, 1, 0, Player.PLAYER_1));
        assertTrue(field.isWinning(0, 2));
        assertTrue(field.isWinning(1, 2));
        assertTrue(field.isWinning(2, 2));
        assertTrue(field.isWinning(3, 2));
    }
    @Test
    public void testIsWinning_bothPlaying_1() {
        Field field = new Field();

        field.put(2, Player.PLAYER_1);
        field.put(2, Player.PLAYER_1);
        field.put(2, Player.PLAYER_2);
        field.put(2, Player.PLAYER_1);
        assertFalse(field.isWinning(0, 2));
        assertFalse(field.isWinning(1, 2));
        assertFalse(field.isWinning(2, 2));
        assertFalse(field.isWinning(3, 2));

        field.put(3, Player.PLAYER_1);
        field.put(3, Player.PLAYER_1);
        field.put(3, Player.PLAYER_2);
        field.put(3, Player.PLAYER_1);
        assertFalse(field.isWinning(0, 3));
        assertFalse(field.isWinning(1, 3));
        assertFalse(field.isWinning(2, 3));
        assertFalse(field.isWinning(3, 3));

        field.put(4, Player.PLAYER_1);
        field.put(4, Player.PLAYER_1);
        field.put(4, Player.PLAYER_2);
        field.put(4, Player.PLAYER_1);

        field.put(5, Player.PLAYER_2);
        field.put(5, Player.PLAYER_2);
        field.put(5, Player.PLAYER_1);
        field.put(5, Player.PLAYER_1);
        assertFalse(field.isWinning(0, 5));
        assertFalse(field.isWinning(1, 5));
        assertFalse(field.isWinning(2, 5));
        assertTrue(field.isWinning(3, 5));
    }
    @Test
    public void testIsWinning_copy_makesWinner() {
        Field field = new Field();

        field.put(2, Player.PLAYER_1);
        field.put(2, Player.PLAYER_1);
        field.put(2, Player.PLAYER_2);
        field.put(2, Player.PLAYER_1);

        field.put(3, Player.PLAYER_1);
        field.put(3, Player.PLAYER_1);
        field.put(3, Player.PLAYER_2);
        field.put(3, Player.PLAYER_1);

        field.put(4, Player.PLAYER_1);
        field.put(4, Player.PLAYER_1);
        field.put(4, Player.PLAYER_2);
        field.put(4, Player.PLAYER_1);

        field.put(5, Player.PLAYER_2);
        field.put(5, Player.PLAYER_2);
        field.put(5, Player.PLAYER_1);

        assertFalse(field.isWinning(3, 2, 0, 1, Player.PLAYER_2));
        assertFalse(field.isWinning(3, 3, 0, 1, Player.PLAYER_2));
        assertFalse(field.isWinning(3, 4, 0, 1, Player.PLAYER_2));
        assertFalse(field.isWinning(3, 5, 0, 1, Player.PLAYER_2));
        assertFalse(field.isWinning(3, 2));
        assertFalse(field.isWinning(3, 3));
        assertFalse(field.isWinning(3, 4));
        assertNull(field.getWinner());

        Field copyField = field.copyAndPut(5, Player.PLAYER_1);
        System.out.println(copyField.getMatrixString());
        System.out.println("copyField.getWinner()="+copyField.getWinner());

        assertTrue(copyField.isWinning(3, 2, 0, 1, Player.PLAYER_1));
        assertTrue(copyField.isWinning(3, 3, 0, 1, Player.PLAYER_1));
        assertTrue(copyField.isWinning(3, 4, 0, 1, Player.PLAYER_1));
        assertTrue(copyField.isWinning(3, 5, 0, 1, Player.PLAYER_1));
        assertTrue(copyField.isWinning(3, 2));
        assertTrue(copyField.isWinning(3, 3));
        assertTrue(copyField.isWinning(3, 4));
        assertTrue(copyField.isWinning(3, 5));
        assertEquals(Player.PLAYER_1, copyField.getWinner());
    }
}
