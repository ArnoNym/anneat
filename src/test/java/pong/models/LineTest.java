package pong.models;

import org.junit.Test;
import pong.Constants;

import static org.junit.Assert.*;

public class LineTest {
    @Test
    public void testHorizontalLine() {
        HorizontalLine line = new HorizontalLine(0, 10, 10);

        Vector2 position = new Vector2(5, 20);
        assertTrue(line.isPositionAbove(position));
        assertFalse(line.isPositionBelow(position));

        position = new Vector2(20, 20);
        assertTrue(line.isPositionAbove(position));
        assertFalse(line.isPositionBelow(position));

        position = new Vector2(5, 1);
        assertFalse(line.isPositionAbove(position));
        assertTrue(line.isPositionBelow(position));
    }

    @Test
    public void testVerticalLine() {
        VerticalLine line = new VerticalLine(10, 10, 30);

        Vector2 position = new Vector2(5, 20);
        assertTrue(line.isPositionLeft(position));
        assertFalse(line.isPositionRight(position));

        position = new Vector2(5, 40);
        assertTrue(line.isPositionLeft(position));
        assertFalse(line.isPositionRight(position));

        position = new Vector2(15, 20);
        assertFalse(line.isPositionLeft(position));
        assertTrue(line.isPositionRight(position));
    }

    @Test
    public void more() {
        VerticalLine leftLine = new VerticalLine(
                Constants.LEFT_BOARDER_X,
                Constants.TOP_BOARDER_Y,
                Constants.BOTTOM_BOARDER_Y);

        HorizontalLine player1Line = new HorizontalLine(
                Constants.LEFT_BOARDER_X,
                Constants.RIGHT_BOARDER_X,
                Constants.BOTTOM_BOARDER_Y);

        VerticalLine rightLine = new VerticalLine(
                Constants.RIGHT_BOARDER_X,
                Constants.TOP_BOARDER_Y,
                Constants.BOTTOM_BOARDER_Y);

        HorizontalLine player2Line = new HorizontalLine(
                Constants.LEFT_BOARDER_X,
                Constants.RIGHT_BOARDER_X,
                Constants.TOP_BOARDER_Y);

        Vector2 position = new Vector2(252.5, 252.5);

        assertFalse(leftLine.isPositionLeft(position));
        assertFalse(player1Line.isPositionAbove(position));
        assertFalse(rightLine.isPositionRight(position));
        assertFalse(player2Line.isPositionBelow(position));

        assertTrue(leftLine.isPositionRight(position));
        assertTrue(player1Line.isPositionBelow(position));
        assertTrue(rightLine.isPositionLeft(position));
        assertTrue(player2Line.isPositionAbove(position));
    }
}
