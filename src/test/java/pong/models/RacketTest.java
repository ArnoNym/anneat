package pong.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class RacketTest {
    @Test
    public void testHorizontalLine() {
        Racket racket = new Racket(10, 20, 10);

        Vector2 above = new Vector2(15, 15);
        Vector2 below = new Vector2(15, 5);

        assertTrue(racket.isPositionAbove(above));
        assertTrue(racket.isPositionBelow(below));

        assertTrue(racket.isMovedBelow(above, below));
        assertTrue(racket.isMovedAbove(below, above));
    }
}
