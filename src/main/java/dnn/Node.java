package dnn;

import org.jetbrains.annotations.Nullable;
import utils.Functions;

import java.util.Set;

public class Node {
    private double value;

    private final Set<Connection> connections;

    public Node(@Nullable Set<Connection> connections) {
        this.connections = connections;
    }

    public void calculate() {
        if (connections == null) {
            throw new IllegalStateException();
        }

        value = 0;
        for (Connection connection : connections) {
            value += connection.getOrigin().getValue() * connection.getWeight();
        }
        value = Functions.sigmoid(value);
    }

    public void setValue(double value) {
        if (connections != null) {
            throw new IllegalStateException();
        }

        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
