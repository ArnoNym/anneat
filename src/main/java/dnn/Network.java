package dnn;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class Network {
    private final List<Node> inputLayer;
    private final List<Node> outputLayer;
    private final List<Node> hiddenLayers = new ArrayList<>();
    private final List<Connection> connections = new ArrayList<>();

    public Network(int[] layerSizes) {
        if (layerSizes.length < 2) {
            throw new IllegalArgumentException();
        }

        this.inputLayer = new ArrayList<>(createLayer(null, layerSizes[0]));

        Collection<Node> previousLayer = inputLayer;
        for (int i = 1; i < layerSizes.length - 2; i++) {
            previousLayer = createLayer(previousLayer, layerSizes[i]);
        }

        this.outputLayer = new ArrayList<>(createLayer(previousLayer, layerSizes[layerSizes.length - 1]));
    }

    @NotNull
    private Set<Node> createLayer(@Nullable Collection<Node> previousLayer, int layerSize) {
        Set<Node> layer = new HashSet<>();

        for (int i = 0; i < layerSize; i++) {
            Set<Connection> connections = new HashSet<>();

            if (previousLayer != null) {
                for (Node node : previousLayer) {
                    Connection connection = new Connection(node);
                    connections.add(connection);
                    this.connections.add(connection);
                }
            }

            layer.add(new Node(connections));
        }

        this.hiddenLayers.addAll(layer);

        return layer;
    }

    public double[] calculate(@NotNull double[] input) {
        if (input.length != inputLayer.size()) {
            throw new IllegalArgumentException();
        }

        for (int i = 0; i < input.length; i++) {
            inputLayer.get(i).setValue(input[i]);
        }

        for (Node node : hiddenLayers) {
            node.calculate();
        }

        double[] output = new double[outputLayer.size()];
        for (int i = 0, outputLayerSize = outputLayer.size(); i < outputLayerSize; i++) {
            Node node = outputLayer.get(i);
            node.calculate();
            output[i] = node.getValue();
        }
        return output;
    }
}
