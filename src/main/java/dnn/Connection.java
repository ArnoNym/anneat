package dnn;

public class Connection {
    private final Node origin;
    private double weight;

    public Connection(Node origin) {
        this.origin = origin;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void updateWeight(double learningRate, double lossFunctionDerivationToThisWeight) {
        this.weight = this.weight - learningRate * lossFunctionDerivationToThisWeight;
    }

    public Node getOrigin() {
        return origin;
    }
}
