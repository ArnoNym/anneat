package four.controller;

import four.models.Game;

public class GameController {
    private final Game game;

    public GameController() {
        this.game = new Game();
    }

    public Game getGame() {
        return game;
    }
}
