package four.models;

import four.Constants;

import java.util.HashSet;
import java.util.Set;

public class Matrix {
    private final Player[][] columns = new Player[Constants.COLUMN_COUNT][Constants.ROW_COUNT];

    public Matrix() {
        for (int column = 0; column < Constants.COLUMN_COUNT; column++) {
            Player[] row = columns[column];
            for (int field = 0; field < Constants.ROW_COUNT; field++) {
                row[field] = null;
            }
        }
    }

    public Matrix copy() {
        Matrix matrix = new Matrix();
        for (int c = 0; c < Constants.COLUMN_COUNT; c++) {
            Player[] row = matrix.columns[c];
            System.arraycopy(columns[c], 0, row, 0, Constants.ROW_COUNT);
        }
        return matrix;
    }

    public boolean isValidPut(int row, int column) {
        return get(row, column) == null;
    }

    public void put(int row, int column, Player player) {
        assert isValidPut(row, column);
        columns[column][row] = player;
    }

    public Player get(int row, int column) {
        return columns[column][row];
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int rowIndex = Constants.ROW_COUNT - 1; rowIndex >= 0; rowIndex--) {
            for (int columnIndex = 0; columnIndex < Constants.COLUMN_COUNT; columnIndex++) {
                s.append(get(rowIndex, columnIndex)).append(", ");
            }
            s.append("\n");
        }
        return s.toString();
    }
}
