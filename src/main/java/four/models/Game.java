package four.models;

public class Game {
    private Field field;
    private Player player = Player.PLAYER_1;

    public Game() {
        this.field = new Field();
    }

    public void togglePlayer() {
        if (player.equals(Player.PLAYER_2)) {
            player = Player.PLAYER_1;
        } else {
            player = Player.PLAYER_2;
        }
    }

    public Player getPlayer() {
        return player;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }
}
