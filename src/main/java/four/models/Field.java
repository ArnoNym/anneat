package four.models;

import four.Constants;

import java.util.HashSet;
import java.util.Set;

public class Field {
    private final Matrix matrix;

    private Player winner = null;

    public Field() {
        this.matrix = new Matrix();
    }

    public Field(Matrix matrix) {
        this.matrix = matrix;
    }

    public Field copyAndPut(final int columnIndex, Player player) {
        Matrix matrix = this.matrix.copy();
        Integer rowIndex = getRowIndex(columnIndex);
        if (rowIndex == null) {
            return null;
        }
        matrix.put(rowIndex, columnIndex, player);
        Field field = new Field(matrix);
        //System.out.println("Field.copyAndPut ::: ");
        //System.out.println(matrix);
        if (field.isWinning(rowIndex, columnIndex)) {
            field.winner = field.get(rowIndex, columnIndex);
        } else {
            field.winner = null;
        }
        //System.out.println("field.winner="+field.winner);
        return field;
    }

    public Set<Field> calcPossibleFutureFields(Player player) {
        Set<Field> fields = new HashSet<>();
        for (int columnIndex = 0; columnIndex < Constants.COLUMN_COUNT; columnIndex++) {
            if (isValidPut(columnIndex)) {
                Field field = copyAndPut(columnIndex, player);
                fields.add(field);
            }
        }
        return fields;
    }

    public boolean isWinning(final int row, final int column) {
        Player player = get(row, column);
        assert player != null;
        return isWinning(row, column, 1, 0, player)
                || isWinning(row, column, 0, 1, player)
                || isWinning(row, column, 1, 1, player)
                || isWinning(row, column, 1, -1, player);
    }

    public boolean isWinning(final int row, final int column, int rowAdd, int columnAdd, Player player) {
        int r = row + rowAdd;
        int c = column + columnAdd;
        int count = 1;
        //System.out.println("\n##############################################\n\n"+matrix);
        while (r >= 0 && c >= 0 && r < Constants.ROW_COUNT && c < Constants.COLUMN_COUNT) {
            Player val = get(r, c);
            //System.out.println("Field.isWinning ::: r="+r+", c="+c+", val = "+val);
            if (val == null || val != player) {
                break;
            }
            count++;
            r += rowAdd;
            c += columnAdd;
        }
        if (count >= Constants.WINNING_COUNT) {
            return true;
        }
        r = row - rowAdd;
        c = column - columnAdd;
        while (r >= 0 && c >= 0 && r < Constants.ROW_COUNT && c < Constants.COLUMN_COUNT) {
            Player val = get(r, c);
            if (val == null || val != player) {
                break;
            }
            count++;
            r -= rowAdd;
            c -= columnAdd;
        }
        return count >= Constants.WINNING_COUNT;
    }

    public boolean isValidPut(int column) {
        if (column < 0 || column >= Constants.COLUMN_COUNT) {
            return false;
        }
        int row = Constants.ROW_COUNT - 1;
        while (true) {
            if (get(row, column) == null) {
                return true;
            }
            row--;
            if (row < 0) {
                return false;
            }
        }
    }

    public Integer getRowIndex(int columnIndex) {
        int rowIndex = 0;
        while (get(rowIndex, columnIndex) != null) {
            rowIndex++;
            if (rowIndex >= Constants.ROW_COUNT) {
                return null;
            }
        }
        return rowIndex;
    }

    public void put(int columnIndex, Player player) {
        assert isValidPut(columnIndex);
        Integer rowIndex = getRowIndex(columnIndex);
        System.out.println("Field.put ::: rowIndex="+rowIndex+", columnIndex="+columnIndex);
        matrix.put(rowIndex, columnIndex, player);
    }

    public Player get(int row, int column) {
        return matrix.get(row, column);
    }

    public Player getWinner() {
        return winner;
    }

    public String getMatrixString() {
        return matrix.toString();
    }
}
