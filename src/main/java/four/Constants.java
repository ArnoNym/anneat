package four;

public class Constants {
    public static final int ROW_COUNT = 6;
    public static final int COLUMN_COUNT = 7;
    public static final int WINNING_COUNT = 4;
}
