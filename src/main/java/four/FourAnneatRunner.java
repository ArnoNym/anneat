package four;

import anneat.main.SerializableFunction;
import anneat.main.utils.Utils;
import anneat.main.utils.arena.Gladiator;
import anneat.models.calculation.Calculator;
import anneat.models.generation.Agent;
import anneat.publish.Neat;
import anneat.publish.NeatAgent;
import anneat.publish.Serializer;
import anneat.publish.settings.Settings;
import anneat.publish.task.Deterministic;
import anneat.publish.task.SoloTask;
import anneat.publish.task.deprecatedArena.Fight;
import four.controller.GameController;
import four.models.Field;
import four.models.Player;

import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class FourAnneatRunner {

    public static void main(String[] args) {
        train(true);
        //loadAndLetPlay();
    }

    public static void train(boolean save) {
        //ArenaTask task = new ArenaTask(Constants.ROW_COUNT * Constants.COLUMN_COUNT * 2, 1, 1, createFight());
        SoloTask task = new SoloTask(Constants.ROW_COUNT * Constants.COLUMN_COUNT * 2, 1, Deterministic.YES_PERFORMANCE, createTask(100));

        Settings settings = new Settings();
        settings.getPerformanceSettings().setPrint(true);
        //settings.setMaxClientCount(100);
        Neat neat = new Neat(settings, task, 48d, null);
        neat.evolve(30);

        if (save) {
            Serializer.save(new NeatAgent(neat.getBestAgent().getCalculator().getFunction()), "fourAgents");
        }

        List<Agent> agentList = neat.getBestAgents();
        Agent bestAgent = agentList.get(0);
        Agent secondBestAgent = agentList.get(0);
        assert bestAgent.getVanillaScore() >= secondBestAgent.getVanillaScore();

        new anneat.view.Frame(bestAgent);
        new anneat.view.Frame(secondBestAgent);
        showMatch(bestAgent.getCalculator(), secondBestAgent.getCalculator());
    }

    public static void loadAndLetPlay() {
        NeatAgent neatAgent = Serializer.load("fourAgents");
        assert neatAgent != null;
        showMatch(neatAgent.getCalculator(), neatAgent.getCalculator());
    }

    public static void showMatch(Calculator calculator1, Calculator calculator2) {
        GameController gameController = new GameController();

        Frame frame = new Frame(gameController.getGame());

        while (true) {
            Calculator calculator;
            if (gameController.getGame().getPlayer().equals(Player.PLAYER_1)) {
                calculator = calculator1;
            } else {
                calculator = calculator2;
            }
            boolean draw = takeTurn(gameController, calculator.getFunction());
            if (draw) {
                System.out.println("Draw!");
                return;
            }
            Player winner = gameController.getGame().getField().getWinner();
            if (winner != null) {
                System.out.println("We have a winner! Player"+winner+"!");
                return;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            frame.repaint();
        }
    }

    public static SerializableFunction<SerializableFunction<double[], double[]>, Double> createTask(final int matches) {
        SerializableFunction<double[], double[]> randomFunction = Utils.createRandomFunction(1);
        return function -> {
            double score = 0;
            for (int i = 0; i < matches; i++){
                GameController gameController = new GameController();
                gameController.getGame().togglePlayer();
                while (true) {
                    Function<double[], double[]> currentFunction;
                    if (gameController.getGame().getPlayer().equals(Player.PLAYER_1)) {
                        currentFunction = function;
                    } else {
                        currentFunction = randomFunction;
                    }

                    boolean draw = takeTurn(gameController, currentFunction);

                    if (draw) {
                        score += 0.5;
                        break;
                    }

                    Player winner = gameController.getGame().getField().getWinner();

                    if (winner != null) {
                        if (winner.equals(Player.PLAYER_1)) {
                            score += 1;
                        }
                        break;
                    }
                }
            }
            return score;
        };
    }

    public static Fight createFight() {
        return (gladiator1, gladiator2) -> {
            final int rounds = 10;

            double f1Wins = 0;
            double f2Wins = 0;

            for (int i = 0; i < rounds; i++) {
                GameController gameController = new GameController();
                while (true) {
                    Gladiator currentGladiator;
                    if (gameController.getGame().getPlayer().equals(Player.PLAYER_1)) {
                        currentGladiator = gladiator1;
                    } else {
                        currentGladiator = gladiator2;
                    }

                    boolean draw = takeTurn(gameController, currentGladiator.getCalculator().getFunction());

                    if (draw) {
                        f1Wins += 0.5;
                        f2Wins += 0.5;
                        break;
                    }

                    Player winner = gameController.getGame().getField().getWinner();

                    if (winner != null) {
                        if (winner.equals(Player.PLAYER_1)) {
                            f1Wins += 1;
                        } else {
                            f2Wins += 1;
                        }
                        break;
                    }
                }
            }
            double f1Score;
            double f2Score;
            if (f1Wins == rounds) {
                f1Score = 1;
                f2Score = 0;
            } else if (f2Wins == rounds) {
                f1Score = 0;
                f2Score = 1;
            } else {
                f1Score = 0;
                f2Score = 0;
            }
            gladiator1.setScore(f1Score);
            gladiator2.setScore(f2Score);
        };
    }

    public static boolean takeTurn(GameController gameController, Function<double[], double[]> function) {
        Set<Field> possibleFields = gameController.getGame().getField().calcPossibleFutureFields(gameController.getGame().getPlayer());
        if (possibleFields.isEmpty()) {
            return true;
        }
        Field bestField = null;
        double bestFieldScore = -Double.MAX_VALUE;
        for (Field field : possibleFields) {
            double fieldScore = function.apply(createInput(field, gameController.getGame().getPlayer()))[0];
            if (fieldScore > bestFieldScore) {
                bestFieldScore = fieldScore;
                bestField = field;
            }
        }
        gameController.getGame().setField(bestField);
        gameController.getGame().togglePlayer();
        return false;
    }

    public static double[] createInput(Field field, Player player) {
        double[] input = new double[Constants.ROW_COUNT * Constants.COLUMN_COUNT * 2];

        for (int columnIndex = 0; columnIndex < Constants.COLUMN_COUNT; columnIndex++) {
            for (int rowIndex = 0; rowIndex < Constants.ROW_COUNT; rowIndex++) {
                double p1InputValue;
                double p2InputValue;
                Player fieldValue = field.get(rowIndex, columnIndex);
                if (fieldValue == null) {
                    p1InputValue = 0;
                    p2InputValue = 0;
                } else if (fieldValue.equals(player)) {
                    p1InputValue = 1;
                    p2InputValue = 0;
                } else {
                    p1InputValue = 0;
                    p2InputValue = 1;
                }
                input[columnIndex + rowIndex] = p1InputValue;
                input[1 + (columnIndex + rowIndex) * 2] = p2InputValue;
            }
        }

        return input;
    }
}
