package four;

import four.models.Game;
import four.models.Player;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class Panel extends JPanel {
    private final Map<Integer, Map<Integer, JButton>> rowColumnButtonMap = new HashMap<>();

    private final Frame frame;
    private final Game game;

    public Panel(Frame frame, Game game) {
        this.frame = frame;
        this.game = game;

        for (int rowIndex = 0; rowIndex < Constants.ROW_COUNT; rowIndex++) {
            rowColumnButtonMap.put(rowIndex, new HashMap<>());
        }

        JPanel menu = new JPanel();
        menu.setPreferredSize(new Dimension(500,500));
        menu.setLayout(new GridLayout(Constants.ROW_COUNT, Constants.COLUMN_COUNT));

        for (int rowIndex = Constants.ROW_COUNT - 1; rowIndex >= 0; rowIndex--) {
            for (int columnIndex = 0; columnIndex < Constants.COLUMN_COUNT; columnIndex++) {

                JButton button = new JButton();

                Player val = game.getField().get(rowIndex, columnIndex);
                if (val == null) {
                    button.setBackground(Color.BLACK);
                } else if (val.equals(Player.PLAYER_1)) {
                    button.setBackground(Color.RED);
                } else {
                    button.setBackground(Color.GREEN);
                }
                button.setOpaque(true);
                button.setBorderPainted(false);

                final int finalColumnIndex = columnIndex;
                button.addActionListener(e -> {
                    game.getField().put(finalColumnIndex, game.getPlayer());
                    game.togglePlayer();
                    frame.repaint();
                });

                menu.add(button);
                rowColumnButtonMap.get(rowIndex).put(columnIndex, button);
            }
        }
        add(menu);
    }

    @Override
    protected void paintComponent(Graphics g) {
        for (int rowIndex = Constants.ROW_COUNT - 1; rowIndex >= 0; rowIndex--) {
            for (int columnIndex = 0; columnIndex < Constants.COLUMN_COUNT; columnIndex++) {
                JButton button = rowColumnButtonMap.get(rowIndex).get(columnIndex);
                Player val = game.getField().get(rowIndex, columnIndex);
                if (val == null) {
                    button.setBackground(Color.BLACK);
                } else if (val.equals(Player.PLAYER_1)) {
                    button.setBackground(Color.RED);
                } else {
                    button.setBackground(Color.GREEN);
                }
                button.setOpaque(true);
                button.setBorderPainted(false);
            }
        }
    }
}
