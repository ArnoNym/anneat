package anneat.publish.task.deprecatedArena;

import anneat.main.utils.arena.Gladiator;

import java.io.Serializable;

@FunctionalInterface
public interface Fight extends Serializable {
    void act(Gladiator gladiator1, Gladiator gladiator2);
}
