package anneat.publish.task;

import anneat.models.calculation.Calculator;

import java.io.Serializable;

public interface BenchmarkFight<BENCHMARK> extends Serializable {
    double act(Calculator active, BENCHMARK benchmarkAgent, double benchmarkScore);
}
