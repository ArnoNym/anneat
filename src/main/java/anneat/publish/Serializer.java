package anneat.publish;

import java.io.*;
import java.util.List;

public class Serializer {
    /*
    public static void save(Neat neat, String name) {
        try {
            String path = "C:\\Nutzer\\Projekte\\anneat\\tmp\\"+name+".ser";
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(neat);
            out.close();
            fileOut.close();
            System.out.print("Serialized data is saved in "+path);
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public static Neat load(String name) {
        try {
            String path = "C:\\Nutzer\\Projekte\\anneat\\tmp\\"+name+".ser";
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            List<NeatAgent> agentList = (List<NeatAgent>) in.readObject();
            in.close();
            fileIn.close();
            return agentList;
        } catch (IOException i) {
            i.printStackTrace();
            return null;
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
            return null;
        }
    }
    */

    public static void save(NeatAgent neatAgent, String name) {
        try {
            String path = "C:\\Nutzer\\Projekte\\anneat\\tmp\\"+name+".ser";
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(neatAgent);
            out.close();
            fileOut.close();
            System.out.print("Serialized data is saved in "+path);
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public static NeatAgent load(String name) {
        try {
            String path = "C:\\Nutzer\\Projekte\\anneat\\tmp\\"+name+".ser";
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            NeatAgent neatAgent = (NeatAgent) in.readObject();
            in.close();
            fileIn.close();
            return neatAgent;
        } catch (IOException i) {
            i.printStackTrace();
            return null;
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
            return null;
        }
    }
}
