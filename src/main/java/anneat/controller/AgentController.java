package anneat.controller;

import anneat.models.generation.Agent;

public abstract class AgentController {
    public static void cleanHistory(Agent agent) {
        agent.setAllowKill(true);
        agent.setAllowMutate(true);
        agent.setCalculator(null);
    }
}
