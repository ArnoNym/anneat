package anneat.controller;

import anneat.main.utils.collections.OrderedSet;
import anneat.models.generation.Agent;
import anneat.models.generation.Generation;
import anneat.models.generation.Species;

import java.util.List;

public abstract class GenerationController {
    public static void createNewSpecies(Generation generation) {
        List<Agent> agents = generation.getAgentList();
        OrderedSet<Species> species = generation.getOrderedSet();
        species.clear();
        species.add(new Species(agents));
        EvolutionController.redistributeAgentsInSpecies(species);
    }
}
