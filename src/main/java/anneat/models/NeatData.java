package anneat.models;

import anneat.controller.AgentController;
import anneat.controller.GenerationController;
import anneat.models.generation.Agent;
import anneat.models.generation.Generation;
import anneat.models.genome.Pool;
import anneat.publish.settings.EvolutionSettings;
import anneat.publish.settings.PerformanceSettings;
import anneat.publish.settings.Settings;
import anneat.publish.task.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

public class NeatData implements Serializable {
    private final Generation generation;
    private final Pool pool; // Can be calculated from the generation

    private Task task;
    private Double targetScore;

    public NeatData(@NotNull Task task, @Nullable Double targetScore) {
        this(new Settings(new PerformanceSettings(), new EvolutionSettings()), task, targetScore);
    }

    public NeatData(@NotNull Settings settings, @NotNull Task task, @Nullable Double targetScore) {
        this.task = task;

        this.targetScore = targetScore;

        this.pool = new Pool(task.getInputSize(), task.getOutputSize(), settings);

        this.generation = Generation.init(pool);

        task.evolvedGeneration(generation.getAgentList());
    }

    public Settings getSettings() {
        return pool.getSettings();
    }

    public void setSettings(Settings settings) {
        pool.setSettings(settings);
    }

    public Generation getGeneration() {
        return generation;
    }

    public Pool getPool() {
        return pool;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Double getTargetScore() {
        return targetScore;
    }

    public void setTargetScore(Double targetScore) {
        this.targetScore = targetScore;
    }
}
