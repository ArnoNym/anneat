package tiefensuche;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

public abstract class State<S extends State<?>> {
    private final String gameState;

    private S nextPassGameState;
    private S nextPatch1GameState;
    private S nextPatch2GameState;
    private S nextPatch3GameState;

    public State(String gameState) {
        this.gameState = gameState;
    }

    public abstract double getRating();

    protected void setNextStates(S nextPassGameState, S nextPatch1GameState,
                                 S nextPatch2GameState, S nextPatch3GameState) {
        this.nextPassGameState = nextPassGameState;
        this.nextPatch1GameState = nextPatch1GameState;
        this.nextPatch2GameState = nextPatch2GameState;
        this.nextPatch3GameState = nextPatch3GameState;
    }

    public String getGameState() {
        return gameState;
    }

    public S getNextPassGameState() {
        return nextPassGameState;
    }

    public S getNextPatch1GameState() {
        return nextPatch1GameState;
    }

    public S getNextPatch2GameState() {
        return nextPatch2GameState;
    }

    public S getNextPatch3GameState() {
        return nextPatch3GameState;
    }

    public Set<S> getNextStates() {
        Set<S> nextStates = new HashSet<>();
        nextStates.add(nextPassGameState);
        nextStates.add(nextPatch1GameState);
        nextStates.add(nextPatch2GameState);
        nextStates.add(nextPatch2GameState);
        return nextStates;
    }
}
