package tiefensuche;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class StateController {
    private final List<MyActionState> actionStates = new ArrayList<>();
    private final List<MyState> unusedStates = new ArrayList<>();

    public StateController(String gameState, Function<String, Double> rateGameStateFunction) {
        createActionState(gameState, rateGameStateFunction, Action.PASS);
        createActionState(gameState, rateGameStateFunction, Action.PATCH_1);
        createActionState(gameState, rateGameStateFunction, Action.PATCH_2);
        createActionState(gameState, rateGameStateFunction, Action.PATCH_3);
    }

    public void createActionState(String currentGameState, Function<String, Double> rateGameStateFunction, Action action) {
        String copyCurrentGameState = currentGameState + ".copy()"; // TODO
        switch (action) {
            case PASS:
                // TODO pass
                break;
            case PATCH_1:
                // TODO patch 1
                break;
            case PATCH_2:
                // TODO patch 2
                break;
            case PATCH_3:
                // TODO patch 3
                break;
            default:
                throw new IllegalStateException();
        }
        MyActionState actionState = new MyActionState(copyCurrentGameState, rateGameStateFunction.apply(currentGameState), action);
        actionStates.add(actionState);
        unusedStates.add(actionState);
    }

    public String getBestGameState() {
        return actionStates.stream().max(Comparator.comparingDouble(MyState::getRating)).orElseThrow().getGameState();
    }

    public Action getBestAction() {
        return actionStates.stream().max(Comparator.comparingDouble(MyState::getRating)).orElseThrow().getTurn();
    }

    public void compute(int count) {
        while (count > 0) {
            compute();
            count--;
        }
    }

    public void compute() {
        unusedStates.sort(Comparator.comparingDouble(State::getRating));
        assert unusedStates.size() < 2 || unusedStates.get(0).getRating() <= unusedStates.get(1).getRating();
        MyState bestUnusedState = unusedStates.remove(unusedStates.size() - 1);
        compute(bestUnusedState);
        bestUnusedState.getNextStates().forEach(enemyState -> {
            compute(enemyState);
            Set<MyState> myNextStates = enemyState.getNextStates();
            myNextStates.forEach(this::compute);
            unusedStates.addAll(myNextStates);
        });
    }

    private void compute(State<?> state) {
        /* TODO
        state.setNextStates(
                state.pass,
                state.patch1,
                state.patch2,
                state.patch3
        );
        */
    }
}
