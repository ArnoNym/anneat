package tiefensuche;

public class MyState extends State<EnemyState> {
    private final double rating;

    public MyState(String gameState, double rating) {
        super(gameState);
        this.rating = rating;
    }

    public double getRating() {
        if (getNextPassGameState() == null) {
            return rating;
        } else {
            assert getNextPatch1GameState() != null;
            assert getNextPatch2GameState() != null;
            assert getNextPatch3GameState() != null;
            return getNextPassGameState().getRating() * getNextPassGameState().getProbability()
                    + getNextPatch1GameState().getRating() * getNextPatch1GameState().getProbability()
                    + getNextPatch2GameState().getRating() * getNextPatch2GameState().getProbability()
                    + getNextPatch3GameState().getRating() * getNextPatch3GameState().getProbability();
        }
    }
}
