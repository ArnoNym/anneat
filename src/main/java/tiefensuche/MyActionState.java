package tiefensuche;

public class MyActionState extends MyState {
    private final Action action;

    public MyActionState(String gameState, double rating, Action action) {
        super(gameState, rating);
        this.action = action;
    }

    public Action getTurn() {
        return action;
    }
}
