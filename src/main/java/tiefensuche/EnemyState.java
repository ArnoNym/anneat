package tiefensuche;

public class EnemyState extends State<MyState> {
    private final double probability;

    public EnemyState(String gameState, double probability) {
        super(gameState);
        this.probability = probability;
    }

    @Override
    public double getRating() {
        return Math.max(getNextPassGameState().getRating(),
                Math.max(getNextPatch1GameState().getRating(),
                        Math.max(getNextPatch2GameState().getRating(),
                                getNextPatch3GameState().getRating())));
    }

    public double getProbability() {
        return probability;
    }
}
