package pong;

import pong.controller.GameController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Frame extends JFrame {
    private GameController gameController;

    private Panel panel;

    public Frame(GameController gameController) {
        this.gameController = gameController;
        paint();
    }

    @Override
    public void repaint() {
        super.repaint();
    }

    public void paint() throws HeadlessException {
        this.setDefaultCloseOperation(3);

        this.setTitle("NEAT");
        this.setMinimumSize(new Dimension(1000,700));
        this.setPreferredSize(new Dimension(1000,700));

        this.setLayout(new BorderLayout());

        UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
        try {
            UIManager.setLookAndFeel(looks[3].getClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        this.panel = new Panel(gameController.getGame());
        this.add(panel, BorderLayout.CENTER);

        this.setVisible(true);

        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
            }
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                //System.out.println("keyEvent.getKeyCode() == "+keyEvent.getKeyCode());
                if (keyEvent.getKeyCode() == 37) {
                    System.out.println("KEY LEFT PRESSED!");
                    gameController.getPlayer1RacketController().pressLeft();
                    gameController.getPlayer2RacketController().pressLeft();
                } else if (keyEvent.getKeyCode() == 39) {
                    System.out.println("KEY RIGHT PRESSED!");
                    gameController.getPlayer1RacketController().pressRight();
                    gameController.getPlayer2RacketController().pressRight();
                }
            }
            @Override
            public void keyReleased(KeyEvent keyEvent) {
            }
        });
    }
}