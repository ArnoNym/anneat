package pong.controller;

import pong.Constants;
import pong.models.Ball;
import pong.models.Game;
import pong.models.Vector2;

public class BallController {
    private final GameController gameController;
    private final Game game;
    private final Ball ball;

    public BallController(GameController gameController) {
        this.gameController = gameController;
        this.game = gameController.getGame();
        this.ball = gameController.getGame().getBall();
    }

    public void update(float delta) {
        Vector2 ballOldPosition = new Vector2(ball.getPosition());

        ball.update(delta);

        if (game.getLeftLine().isPositionLeft(ball.getPosition())) {
            if (Constants.print) System.out.println("Hit Left Line!");
            ball.invertVelocityX();
        } else if (game.getRightLine().isPositionRight(ball.getPosition())) {
            if (Constants.print) System.out.println("Hit Right Line!");
            ball.invertVelocityX();
        }

        Vector2 position = ball.getPosition();
        position.setX(Math.max(Constants.LEFT_BOARDER_X, Math.min(position.getX(), Constants.RIGHT_BOARDER_X)));

        if (game.getPlayer1Racket().isMovedBelow(ballOldPosition, ball.getPosition())) {
            ball.invertVelocityY();
            ball.getPosition().setY(game.getPlayer1Racket().getY());
            ball.randomizeVelocity(false);
            game.addTotalHits();
        } else if (game.getPlayer2Racket().isMovedAbove(ballOldPosition, ball.getPosition())) {
            ball.invertVelocityY();
            ball.getPosition().setY(game.getPlayer2Racket().getY());
            ball.randomizeVelocity(true);
            game.addTotalHits();
        }
    }
}
