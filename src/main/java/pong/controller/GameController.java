package pong.controller;

import pong.Constants;
import pong.models.Ball;
import pong.models.Game;

public class GameController {
    private final Game game;

    private final BallController ballController;
    private final RacketController player1RacketController;
    private final RacketController player2RacketController;

    public GameController(Game game) {
        this.game = game;
        this.ballController = new BallController(this);
        this.player1RacketController = new RacketController(this, game.getPlayer1Racket());
        this.player2RacketController = new RacketController(this, game.getPlayer2Racket());
    }

    public void update() {
        update(Constants.DELTA);
    }

    public void update(float delta) {
        if (isPlayerOneDead() || isPlayerTwoDead()) {
            throw new IllegalStateException();
        }
        player1RacketController.update(delta);
        player2RacketController.update(delta);
        ballController.update(delta);
    }

    public boolean isPlayerOneDead() {
        return game.getPlayer1Line().isPositionAbove(game.getBall().getPosition());
    }

    public boolean isPlayerTwoDead() {
        return game.getPlayer2Line().isPositionBelow(game.getBall().getPosition());
    }

    public Game getGame() {
        return game;
    }

    public BallController getBallController() {
        return ballController;
    }

    public RacketController getPlayer1RacketController() {
        return player1RacketController;
    }

    public RacketController getPlayer2RacketController() {
        return player2RacketController;
    }
}
