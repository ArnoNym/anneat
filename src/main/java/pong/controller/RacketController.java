package pong.controller;

import pong.Constants;
import pong.models.Ball;
import pong.models.Direction;
import pong.models.Racket;

public class RacketController {
    private final GameController gameController;
    private final Racket racket;

    private Direction direction = Direction.NONE;

    public RacketController(GameController gameController, Racket racket) {
        this.gameController = gameController;
        this.racket = racket;
    }

    public void update(float delta) {
        switch (direction) {
            case LEFT:
                if (Constants.print) System.out.println("Move racket to the LEFT!");
                racket.move(-Constants.RACKET_MOVEMENT_SPEED * delta);
                break;
            case RIGHT:
                if (Constants.print) System.out.println("Move racket to the RIGHT!");
                racket.move(Constants.RACKET_MOVEMENT_SPEED * delta);
                break;
            case NONE:
                break;
            default:
                throw new IllegalStateException();
        }
        direction = Direction.NONE;
    }

    public void pressLeft() {
        direction = Direction.LEFT;
    }

    public void pressRight() {
        direction = Direction.RIGHT;
    }
}
