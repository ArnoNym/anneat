package pong;

public class Constants {
    public static boolean print = false;

    public static final int THREAD_SLEEP = 20;
    public static final int TICKS = 50;
    public static final float DELTA = 1f;

    public static final float BALL_MOVEMENT_SPEED = 5f;
    public static final float RACKET_MOVEMENT_SPEED = 10;

    public static final float RACKET_WIDTH = 75;

    public static final float FIELD_WIDTH = 500;
    public static final float LEFT_BOARDER_X = 5;
    public static final float RIGHT_BOARDER_X = LEFT_BOARDER_X + FIELD_WIDTH;

    public static final float FIELD_HEIGHT = 500;
    public static final float TOP_BOARDER_Y = 5;
    public static final float BOTTOM_BOARDER_Y = TOP_BOARDER_Y + FIELD_HEIGHT;
}
