package pong;

import anneat.publish.Neat;
import anneat.publish.settings.Settings;
import anneat.publish.task.Deterministic;
import anneat.publish.task.SoloTask;
import pong.controller.GameController;
import pong.models.Game;

import java.util.function.Function;

public class PongAnneatSoloRunner {
    private static final int targetScore = 300;
    private static final int generations = 100;
    private static final int gamesPerAgentPerGeneration = 10;

    public static void main(String[] args) throws InterruptedException {

        SoloTask soloTask = new SoloTask(
                8,
                1,
                Deterministic.NO,
                function -> play(gamesPerAgentPerGeneration, function, false));

        Settings settings = new Settings();
        settings.getPerformanceSettings().setPrint(true);
        Neat neat = new Neat(settings, soloTask, (double) targetScore, null);

        neat.evolve(generations);

        //System.out.println("[Generations: "+neat.getGeneration()+"]");

        new anneat.view.Frame(neat);
        new anneat.view.Frame(neat.getBestAgent());
        play(neat.getBestAgent().getCalculator().getFunction(), true);
    }

    private static double play(int gamesCount, Function<double[], double[]> function, boolean show) {
        double score = 0;
        for (int game = 1; game <= gamesCount; game++) {
            score += play(function, show);
        }
        return score / gamesCount;
    }

    private static double play(Function<double[], double[]> function, boolean show) {
        Game game = new Game();
        GameController gameController = new GameController(game);

        Frame frame = null;
        if (show) {
            frame = new Frame(gameController);
        }

        while (game.getTotalHits() < targetScore * 1.5 // 1,5 so that it doesnt have to reach 300 every time
                && !gameController.isPlayerOneDead()
                && !gameController.isPlayerTwoDead()) {
            double[] input = new double[8];
            input[0] = game.getBall().getPosition().getX();
            input[1] = game.getBall().getPosition().getY();
            input[2] = game.getBall().getVelocity().getX();
            input[3] = game.getBall().getVelocity().getY();
            input[4] = game.getPlayer1Racket().getLeft().getX();
            input[5] = game.getPlayer1Racket().getRight().getX();
            input[6] = game.getPlayer2Racket().getLeft().getX();
            input[7] = game.getPlayer2Racket().getRight().getX();
            double output = function.apply(input)[0];
            if (output < 0.4) {
                gameController.getPlayer1RacketController().pressRight();
                gameController.getPlayer2RacketController().pressRight();
            } else if (output > 0.6) {
                gameController.getPlayer1RacketController().pressLeft();
                gameController.getPlayer2RacketController().pressLeft();
            }
            gameController.update();

            if (show) {
                frame.repaint();
                try {
                    Thread.sleep(Constants.THREAD_SLEEP);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        if (show) System.out.println("Score = "+game.getTotalHits());
        return game.getTotalHits();
    }

    public static double xCoordinateToActivation(double x) {
        x = x - Constants.LEFT_BOARDER_X;
        x = Math.max(x, 0);
        x = Math.min(x, Constants.FIELD_WIDTH);
        x = x / Constants.FIELD_WIDTH;
        assert x >= 0 && x <= 1 : "x="+x;
        return x;
    }

    public static double yCoordinateToActivation(double y) {
        y = (y - Constants.TOP_BOARDER_Y) / Constants.FIELD_HEIGHT;
        assert y >= 0 && y <= 1 : "y="+y;
        return y;
    }

    public static double xVelocityToActivation(double x) {
        x = (x + Constants.BALL_MOVEMENT_SPEED) / (2*Constants.BALL_MOVEMENT_SPEED);
        return x;
    }

    public static double yVelocityToActivation(double y) {
        return xVelocityToActivation(y);
    }
}
