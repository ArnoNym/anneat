package pong;

import pong.controller.GameController;
import pong.models.Game;

public class PongPlayerRunner {
    public static void main(String[] args) throws InterruptedException {
        Game game = new Game();
        GameController gameController = new GameController(game);
        Frame frame = new Frame(gameController);
        while (true) {
            Thread.sleep(Constants.THREAD_SLEEP);
            gameController.update();
            frame.repaint();
        }
    }
}
