package pong.models;

public enum Direction {
    LEFT,
    RIGHT,
    NONE
}
