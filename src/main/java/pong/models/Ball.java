package pong.models;

import pong.Constants;

import java.awt.*;
import java.util.Vector;

public class Ball {
    private final Vector2 position;
    private final Vector2 velocity;

    public Ball(Vector2 position, Vector2 velocity) {
        this.position = position;
        this.velocity = velocity;
    }

    public void update(float delta) {
        position.addX(velocity.getX() * delta);
        position.addY(velocity.getY() * delta);
    }

    public void render(Graphics2D g) {
        g.setColor(Color.BLACK);
        int x = (int) position.getX() - 10;
        int y = (int) position.getY() - 10;
        g.drawOval(x, y, 20, 20);
    }

    public void randomizeVelocity(boolean negativeY) {
        double x = Math.random();
        x = Math.max(0.5, x);
        x = Math.min(0.9, x);
        if (Math.random() > 0.5) {
            x *= -1;
        }
        double y = Math.random();
        y = Math.max(0.9, y);
        y = Math.min(1, y);
        if (negativeY) {
            y *= -1;
        }
        x *= Constants.BALL_MOVEMENT_SPEED;
        y *= Constants.BALL_MOVEMENT_SPEED;
        velocity.setX(x);
        velocity.setY(y);
    }

    public void invertVelocityX() {
        velocity.multiplyX(-1);
    }

    public void invertVelocityY() {
        velocity.multiplyY(-1);
    }

    public Vector2 getPosition() {
        return position;
    }

    public Vector2 getVelocity() {
        return velocity;
    }
}
