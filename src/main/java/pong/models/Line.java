package pong.models;

import java.awt.*;

public class Line {
    private final Vector2 start;
    private final Vector2 end;

    public Line(Vector2 start, Vector2 end) {
        this.start = start;
        this.end = end;
    }

    public void render(Graphics2D g) {
        g.setColor(Color.BLACK);
        g.setStroke(new BasicStroke(3));
        g.drawLine(
                (int) start.getX(),
                (int) start.getY(),
                (int) end.getX(),
                (int) end.getY());
    }

    public boolean isOnLine(Vector2 position) {
        return determinant(start, end, position) == 0;
    }

    protected double determinant(Vector2 a, Vector2 b, Vector2 c) {
        return ((b.getX() - a.getX())*(c.getY() - a.getY()) - (b.getY() - a.getY())*(c.getX() - a.getX()));
    }

    public Vector2 getStart() {
        return start;
    }

    public Vector2 getEnd() {
        return end;
    }
}
