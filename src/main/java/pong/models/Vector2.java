package pong.models;

import pong.Constants;

import java.lang.invoke.ConstantBootstraps;
import java.util.Objects;

public class Vector2 {
    private double x;
    private double y;

    public Vector2(Vector2 copy) {
        this.x = copy.x;
        this.y = copy.y;
    }

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void addX(double x) {
        this.x += x;
    }

    public void addY(double y) {
        this.y += y;
    }

    public void multiplyX(double x) {
        this.x *= x;
    }

    public void multiplyY(double y) {
        this.y *= y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public int hashCode() {
        return (int) ((Constants.RIGHT_BOARDER_X + Constants.FIELD_WIDTH) * x + y);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Vector2)) return false;
        Vector2 other = (Vector2) obj;
        return x == other.x && y == other.y;
    }

    @Override
    public String toString() {
        return "Vector2{x: "+x+", y:"+y+"}";
    }
}
