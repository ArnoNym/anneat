package pong.models;

public class HorizontalLine extends Line {
    public HorizontalLine(Vector2 left, Vector2 right) {
        super(left, right);
        if (left.getY() != right.getY()) {
            throw new IllegalArgumentException();
        }
        if (left.getX() > right.getX()) {
            throw new IllegalArgumentException();
        }
    }

    public HorizontalLine(int leftX, int rightX, int y) {
        this((double) leftX, rightX, y);
    }

    public HorizontalLine(double leftX, double rightX, double y) {
        this(new Vector2(leftX, y), new Vector2(rightX, y));
    }

    public boolean isPositionAbove(Vector2 position) {
        return determinant(getLeft(), getRight(), position) > 0;
    }

    public boolean isPositionBelow(Vector2 position) {
        return determinant(getLeft(), getRight(), position) < 0;
    }

    public Vector2 getLeft() {
        if (getStart().getX() < getEnd().getX()) {
            return getStart();
        }
        return getEnd();
    }

    public Vector2 getRight() {
        if (getStart().getX() >= getEnd().getX()) {
            return getStart();
        }
        return getEnd();
    }

    public double getY() {
        return getStart().getY();
    }
}
