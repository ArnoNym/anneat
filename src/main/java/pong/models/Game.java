package pong.models;

import pong.Constants;

import java.awt.*;

public class Game {
    private final Ball ball;

    private final VerticalLine leftLine;
    private final VerticalLine rightLine;
    private final HorizontalLine player1Line;
    private final HorizontalLine player2Line;

    private final Racket player1Racket;
    private final Racket player2Racket;

    private int totalHits = 0;

    public Game(Ball ball,
                VerticalLine leftLine, VerticalLine rightLine,
                HorizontalLine player1Line, HorizontalLine player2Line,
                Racket player1Racket, Racket player2Racket) {
        this.ball = ball;
        this.leftLine = leftLine;
        this.rightLine = rightLine;
        this.player1Line = player1Line;
        this.player2Line = player2Line;
        this.player1Racket = player1Racket;
        this.player2Racket = player2Racket;
    }

    public Game() {
        this.ball = new Ball(new Vector2(252.5, 452.5), new Vector2(0, 0));
        this.ball.randomizeVelocity(false);

        this.leftLine = new VerticalLine(
                Constants.LEFT_BOARDER_X,
                Constants.TOP_BOARDER_Y,
                Constants.BOTTOM_BOARDER_Y);
        this.player1Line = new HorizontalLine(
                Constants.LEFT_BOARDER_X,
                Constants.RIGHT_BOARDER_X,
                Constants.BOTTOM_BOARDER_Y);
        this.rightLine = new VerticalLine(
                Constants.RIGHT_BOARDER_X,
                Constants.TOP_BOARDER_Y,
                Constants.BOTTOM_BOARDER_Y);
        this.player2Line = new HorizontalLine(
                Constants.LEFT_BOARDER_X,
                Constants.RIGHT_BOARDER_X,
                Constants.TOP_BOARDER_Y);

        this.player1Racket = new Racket(
                Constants.LEFT_BOARDER_X + Constants.FIELD_WIDTH / 2 - Constants.RACKET_WIDTH / 2,
                Constants.LEFT_BOARDER_X + Constants.FIELD_WIDTH / 2 + Constants.RACKET_WIDTH / 2,
                35);
        this.player2Racket = new Racket(
                Constants.LEFT_BOARDER_X + Constants.FIELD_WIDTH / 2 - Constants.RACKET_WIDTH / 2,
                Constants.LEFT_BOARDER_X + Constants.FIELD_WIDTH / 2 + Constants.RACKET_WIDTH / 2,
                475);
    }

    public void update(float delta) {
        ball.update(delta);
    }

    public void render(Graphics2D g) {
        ball.render(g);
        leftLine.render(g);
        rightLine.render(g);
        player1Line.render(g);
        player2Line.render(g);
        player1Racket.render(g);
        player2Racket.render(g);
    }

    public Ball getBall() {
        return ball;
    }

    public VerticalLine getLeftLine() {
        return leftLine;
    }

    public VerticalLine getRightLine() {
        return rightLine;
    }

    public HorizontalLine getPlayer1Line() {
        return player1Line;
    }

    public HorizontalLine getPlayer2Line() {
        return player2Line;
    }

    public Racket getPlayer1Racket() {
        return player1Racket;
    }

    public Racket getPlayer2Racket() {
        return player2Racket;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(int totalHits) {
        this.totalHits = totalHits;
    }

    public void addTotalHits() {
        this.totalHits++;
    }
}
