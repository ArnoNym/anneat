package pong.models;

public class VerticalLine extends Line {
    public VerticalLine(Vector2 bottom, Vector2 top) {
        super(bottom, top);
        if (bottom.getX() != top.getX()) {
            throw new IllegalArgumentException();
        }
        if (bottom.getY() > top.getY()) {
            throw new IllegalArgumentException();
        }
    }

    public VerticalLine(int x, int bottomY, int topY) {
        this((double) x, bottomY, topY);
    }

    public VerticalLine(double x, double bottomY, double topY) {
        this(new Vector2(x, bottomY), new Vector2(x, topY));
    }

    public boolean isPositionRight(Vector2 position) {
        return determinant(getBottom(), getTop(), position) < 0;
    }

    public boolean isPositionLeft(Vector2 position) {
        return determinant(getBottom(), getTop(), position) > 0;
    }

    public Vector2 getBottom() {
        return getStart();
    }

    public Vector2 getTop() {
        return getEnd();
    }

    public double getX() {
        return getStart().getX();
    }
}
