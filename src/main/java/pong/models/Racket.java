package pong.models;

import pong.Constants;

public class Racket extends HorizontalLine {

    public Racket(Vector2 start, Vector2 end) {
        super(start, end);
    }

    public Racket(int startX, int endX, int y) {
        super(startX, endX, y);
    }

    public Racket(double startX, double endX, double y) {
        super(startX, endX, y);
    }

    public boolean isMovedAbove(Vector2 before, Vector2 after) {
        if (before.equals(after)) return true;
        boolean isPositionBelow = isPositionBelow(before);
        boolean isPositionAbove = isPositionAbove(after) || isOnLine(after);
        boolean isVerticallyBetween = isVerticallyBetween(before) || isVerticallyBetween(after);
        //System.out.println("isPositionBelow="+isPositionBelow+", isPositionAbove="+isPositionAbove+", isVerticallyBetween="+isVerticallyBetween);
        return isPositionBelow && isPositionAbove && isVerticallyBetween;
    }

    public boolean isMovedBelow(Vector2 before, Vector2 after) {
        if (before.equals(after)) return true;
        boolean isPositionAbove = isPositionAbove(before);
        boolean isPositionBelow = isPositionBelow(after) || isOnLine(after);
        boolean isVerticallyBetween = isVerticallyBetween(before) || isVerticallyBetween(after);
        //System.out.println("before="+before+", after="+after);
        //System.out.println("isPositionAbove="+isPositionAbove+", isPositionBelow="+isPositionBelow+", isVerticallyBetween="+isVerticallyBetween);
        return isPositionAbove && isPositionBelow && isVerticallyBetween;
    }

    public boolean isVerticallyBetween(Vector2 position) {
        Vector2 left = getLeft();
        Vector2 right = getRight();
        return left.getX() <= position.getX() && position.getX() <= right.getX();
    }

    public void move(double x) {
        // System.out.println("Racket.move x == "+x);

        Vector2 left = getLeft();
        Vector2 right = getRight();

        double newLeftX = left.getX() + x;
        double newRightX = right.getX() + x;

        if (newLeftX < Constants.LEFT_BOARDER_X) {
            left.setX(Constants.LEFT_BOARDER_X);
            right.setX(Constants.LEFT_BOARDER_X + Constants.RACKET_WIDTH);
        } else if (newRightX > Constants.RIGHT_BOARDER_X) {
            left.setX(Constants.RIGHT_BOARDER_X - Constants.RACKET_WIDTH);
            right.setX(Constants.RIGHT_BOARDER_X);
        } else {
            left.setX(newLeftX);
            right.setX(newRightX);
        }
    }
}
