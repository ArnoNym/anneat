package pong;

import pong.models.Game;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {
    private final Game game;

    public Panel(Game game) {
        this.game = game;
    }

    @Override
    protected void paintComponent(Graphics g) {
        game.render((Graphics2D) g);
    }
}
