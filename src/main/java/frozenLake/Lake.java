package frozenLake;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Lake {
    private final int[][] fields;

    public Lake(int sizeX, int sizeY) {
        this.fields = new int[sizeX][sizeY];
    }

    public Collection<Move> calculateMoves(int x, int y) {
        assert x >= 0 && x < fields.length && y >= 0 && y < fields[0].length;
        Set<Move> moves = new HashSet<>();
        if (x != 0) {
            moves.add(Move.LEFT);
        }
        if (x != fields.length - 1) {
            moves.add(Move.RIGHT);
        }
        if (y != 0) {
            moves.add(Move.UP);
        }
        if (y != fields[0].length - 1) {
            moves.add(Move.DOWN);
        }
        return moves;
    }
}
